# Lottery

A webapp for holding lotteries of names, for private use. The app uses a single password for login, making it suitable for shared use between a small group. This helps keep the logic simple.

Features realtime database updates to most components (shared state between users).

Developed with Next.js, React, MUI Core (Joy UI), and Supabase. Deployed to Vercel with Supabase and GitLab integration.

Background CSS adapted from work by Temani Afif. `useInterval` function by Dan Abramov.


## Lottery algorithm

Located in the `Utils.tsx` file under `components`, in the `startLottery` function. The lottery is performed on the client for simplicity for now (TODO: server action).

The lottery picks winners from a set of names. First, there are all the names that exist. Second, there is a subset of these names that are "selected". Selected names are considered to qualify to win (more specifically, they exhaust the requirement for the number of winners specified). However, all names take part in the lottery. This is to somewhat balance out the external conditions that lead to being able to be selected in the first place.

Selected names have a weight of 1 (or 100%) in the lottery, while unselected names have a weight of 0.4 (or 40%). This means that it's 40% as likely for an unselected name to win as it is for a selected name. More on unselected "extra" wins later.

For example, if three names are selected and one additional unselected name remains (1 + 1 + 1 + 0.4), the chance for the unselected name to be picked by the algorithm is 0.4 / 3.4, and the chance for a selected name to be picked is 1 / 3.4. This gives a 40% ratio in total: (0.4 / 3.4) / (1 / 3.4) = 0.4 = 40%. The total chance for an unselected name to win in a lottery is dependent on the number of winners specified.

The algorithm is given a number of wins that it must satisfy. For example, maybe a lottery needs to have one or seven winners. The algorithm will continue to pick winners (according to the math above) from the names to reach this, removing them and their weight from the name pool as they win. As mentioned, only selected names count towards this winner count goal. The lottery ends when enough selected names have won.


### Unselected name "extra" wins and tickets

As only selected names can truly win (if an unselected name was qualified to win, it would have been selected), a ticket compensation is given to unselected winners instead, indicated by a ticket icon. This ticket is saved for the name to use in the next lottery it's selected in.

When a name with a ticket enters a lottery as a selected name, the ticket is spent and the name wins automatically. This way, a ticket is cashed in.

If a name holds the maximum amount of tickets but is unselected, it won't take part in the lottery. The maximum number of tickets is currently one.

If more ticket holders are selected than the required winner count, only ticket holders are able to "truly" win, with both the rest of selected and unselected names still entering the lottery for a chance at a ticket. A ticket is only expended if the ticket holder wins.


### Ticket win chance and maximum tickets

The current values of 40% and a maximum ticket count of one are initial best guesses before any real-world experience. A good next development goal is to create a settings table and expose these variables to the UI.

Higher ticket win chances possibly create a risk of too many tickets being won relative to the average amount of required winners per lottery, leading to tickets losing their purpose if everyone has an abundance of tickets.

High maximum ticket counts may lead to ticket hoarding and to their tactical use at the most opportune situations. This is not intended. If external circumstances require a win, it should be handled outside the lottery app entirely.

Low maximum ticket counts encourage to spend tickets quickly, but run the risk of a name not being fairly compensated if not able to participate for a long period of time.


## Lottery results activity and nullification

Lotteries have two possible time types: morning and afternoon lotteries. These both have cutoff times: 09:00 for morning and 18:00 for afternoon. If a lottery is held after this cutoff time, it's displayed as for the next day.

When a previously held lottery passes its next cutoff time, it's displayed as inactive (indicated by darker colors). When a lottery of a time type is inactive, a new lottery can be held.

A nullification button is displayed under lottery results. This effectively cancels the lottery results, returning spent tickets and shredding won tickets. Only active lotteries can be nullified. This is intended to enable adjusting to unforeseen external conditions.


## Local development

You need Node.js, Git, and Docker Desktop.

Fork, clone, and

```bash
npm install
```

Pull requests welcome.


### Supabase

Run Docker Desktop.

Run the local Supabase instance:

```bash
npx supabase start
```

Go to your local Supabase ([http://localhost:54323](http://localhost:54323)) and:

- Create a local test user for the app
- Enable realtime for all tables
- (Optional) Enable RLS and create policies to allow all operations for authorized users for all tables

### Environment variables

Create your own `.env.local` file in project root directory. Required env vars:
```
ACCESS_PASSWORD= // The main password for accessing the site.
NEXT_PUBLIC_SUPABASE_URL=http://localhost:54321
NEXT_PUBLIC_SUPABASE_ANON_KEY= // From npx supabase start
SUPABASE_USER= // Your local test user
SUPABASE_PASSWORD= // Your test user password
```

### Run server

Run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000).


## Deployment

If you wish to deploy your own, create a Supabase account and go through the same setup steps (todo: migrations). Please note that SUPABASE_USER and SUPABASE_PASSWORD are exposed to the client after passing the site login, so be mindful of your Supabase project user credentials.

Deploy to your preferred platform, exposing the environment variables like in local development.

At least Vercel has a Supabase integration available that keeps Supabase variables up to date.

<details> 
  <summary>Why not use anon key instead for everything with Supabase?</summary>
  In theory, it's not that different to use an anon user key/role instead of a single user for Supabase. The only difference is how the anon key is designed to be used, as it's meant to be used by unauthorized users. As such, it's exposed to the client by default in many examples, including the Vercel integration. The Supabase integration in Vercel manages a list of Supabase environment variables, and manually altering them sounds like a bad idea. (You'd need to make sure it's not available to the client before login if you use it for everything.)
</details>
<br/>

## Known issues

- When running in development mode, starting a lottery sometimes results in an error:
```
Warning: Cannot update a component (`HotReload`) while rendering a different component (`LotteryResults`).
```
<br/>

- Local Supabase Docker containers sometimes fail to start due to their ports already being in use. Rare enough that a full system restart is the easiest way to get around.

- `setInterval` executes at different intervals on different browsers, leading to different animation speeds in the lottery.


## TODO

- Lottery history view with nullification history
- Persistent settings for maximum tickets, ticket win chance, lottery type, lottery winners
- Push notifications
- Lottery logic to server action


## Late night development powered by trance EDM

https://music.youtube.com/playlist?list=PLVbt5iLRhEHUYNGwrFtMQOKSYStoh-3Vk
