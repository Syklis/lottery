'use client'

import authenticate from "@/components/ServerUtils";
import Box from "@mui/joy/Box";
import Button from "@mui/joy/Button";
import Card from "@mui/joy/Card";
import CardActions from "@mui/joy/CardActions";
import Input from "@mui/joy/Input";
import { RedirectType, redirect } from "next/navigation";
import { useFormState } from "react-dom";

export default function Page() {
  const [state, dispatch] = useFormState(authenticate, "initial");

  if (state == "success") {
    redirect("/", RedirectType.replace);
  }

  return (
    <Box
      height="100%"
    >
      <Card sx={{ width: 300, mx: "auto", marginTop: "50%" }}>
        <form action={dispatch}>
          <Input type="password" name="password" placeholder="Password" required />
          <CardActions>
            <Button type="submit">Enter</Button>
          </CardActions>
        </form>
      </Card>
    </Box>
  )
}
