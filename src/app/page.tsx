import LotteryForm from "../components/LotteryForm/LotteryForm";
import { supabaseOnServer } from "../components/Supabase";
import { SupabaseClient } from "@supabase/supabase-js";
import Box from "@mui/joy/Box";

export const revalidate = 0;
export const dynamic = 'force-dynamic';

export default async function Home() {
  const supabaseEmail: string = process.env.SUPABASE_USER!;
  const supabasePassword: string = process.env.SUPABASE_PASSWORD!;
  const supabaseUrl: string = process.env.NEXT_PUBLIC_SUPABASE_URL!;
  const supabaseAnonKey: string = process.env.NEXT_PUBLIC_SUPABASE_ANON_KEY!;

  const supabase: SupabaseClient = await supabaseOnServer(supabaseUrl, supabaseAnonKey, supabaseEmail, supabasePassword);

  const maxLotteryResults = 2;
  const { data } = await supabase.from("entrants").select().order("name");
  const lotteries = (await supabase.from("lotteries").select().order("created_at", { ascending: false }).limit(maxLotteryResults)).data;
  const winners: {[key: number]: any[]} = {};
  if (data !== null && lotteries !== null) {
    for (const l of lotteries) {
      let w = (await supabase.from("entrants_lotteries").select().eq("lottery_id", l.id).order("order_index")).data;
      if (w != null) {
        winners[l.id] = w;
      }
    }
  }

  return (
    <Box
      marginTop="25px"
      marginBottom="50px"
    >
      <LotteryForm
        oldNames={data ?? []}
        oldLotteries={lotteries ?? []}
        oldWinners={winners}
        maxLotteryResults={maxLotteryResults}
        supabaseUrl={supabaseUrl}
        supabaseAnonKey={supabaseAnonKey}
        supabaseEmail={supabaseEmail}
        supabasePassword={supabasePassword}
      />
    </Box>
  );
}
