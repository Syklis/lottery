'use client'

import Card from '@mui/joy/Card';
import CardActions from '@mui/joy/CardActions';
import Button from '@mui/joy/Button';
import NameSelect from "./NameSelect";
import TimeSelect from './TimeSelect';
import Stack from '@mui/joy/Stack';
import WinnerAmountSelect from './WinnerAmountSelect';
import { useEffect, useRef, useState } from 'react';
import { supabaseOnClient } from '../Supabase';
import Add from '@mui/icons-material/Add';
import NewName from './NewName';
import { AuthTokenResponsePassword, SupabaseClient } from '@supabase/supabase-js';
import Box from '@mui/joy/Box';
import LotteryResults from './LotteryResults';
import { sortCheckLotteries, startLottery } from '../Utils';
import Snackbar, { SnackbarTypeMap } from '@mui/joy/Snackbar';
import Typography from '@mui/joy/Typography';
import Divider from '@mui/joy/Divider';
import Chip from '@mui/joy/Chip';
import LotteryStats from './LotteryStats';

export default function LotteryForm({ oldNames, oldLotteries, oldWinners, maxLotteryResults, supabaseUrl, supabaseAnonKey, supabaseEmail, supabasePassword }
    : { oldNames: any[], oldLotteries: any[], oldWinners: {[key: number]: any[]}, maxLotteryResults: number, supabaseUrl: string, supabaseAnonKey: string, supabaseEmail: string, supabasePassword: string }) {
  const [names, setNames] = useState<any[]>(oldNames);
  const [lotteries, setLotteries] = useState<any[]>(oldLotteries);
  const [winners, setWinners] = useState<any>(oldWinners);
  const [addNameOpen, setAddNameOpen] = useState(false);
  const [supabase, setSupabase] = useState<SupabaseClient | null>(null);
  const [nameChanged, setNameChanged] = useState(false);
  const [lotteryChanged, setLotteryChanged] = useState<any>(null);
  const [lotteryEntrantChanged, setLotteryEntrantChanged] = useState(false);
  const lotteryEntrantsChanged = useRef<any[]>([]);
  const namesChanged = useRef<any[]>([]);
  const [maxWinners, setMaxWinners] = useState(1);
  const [timeType, setTimeType] = useState("afternoon");
  const [snackbar, setSnackbar] = useState<{ text: string, color: SnackbarTypeMap["props"]["color"] } | null>(null);

  useEffect(() => {
    if (nameChanged) {
      let newNames = [...names];
      for (let i = namesChanged.current.length - 1; i >= 0; i--) {
        const n = namesChanged.current[i];
        switch (n.eventType) {
          case "INSERT": {
            newNames.push(n.new);
            break;
          }
          case "UPDATE": {
            newNames = newNames.filter((e) => e.name != n.new.name);
            newNames.push(n.new);
            break;
          }
          case "DELETE": {
            newNames = newNames.filter((e) => e.name != n.old.name);
            break;
          }
        }
        namesChanged.current.splice(i, 1);
      }
      newNames.sort((a, b) => { return a.name < b.name ? -1 : 1; });
      setNames(newNames);
      setNameChanged(false);
    }
  }, [nameChanged, names]);

  useEffect(() => {
    if (lotteryChanged != null) {
      switch (lotteryChanged.eventType) {
        case "INSERT": {
          const newLotteries = [lotteryChanged.new, ...lotteries]
          sortCheckLotteries(newLotteries);
          newLotteries.splice(maxLotteryResults);
          setLotteries(newLotteries);
          break;
        }
        case "UPDATE": {
          let newLotteries = lotteries.filter((e) => e.id != lotteryChanged.new.id);
          newLotteries.push(lotteryChanged.new);
          sortCheckLotteries(newLotteries);
          setLotteries(newLotteries);
          break;
        }
        case "DELETE": {
          const newLotteries = lotteries.filter((e) => e.id != lotteryChanged.old.id);
          setLotteries(newLotteries);
          setSnackbar({ text: "Lottery deleted", color: "neutral" });
          break;
        }
      }
      setLotteryChanged(null);
    }
  }, [lotteryChanged, lotteries]);

  useEffect(() => {
    if (lotteryEntrantChanged) {
      for (let i = lotteryEntrantsChanged.current.length - 1; i >= 0; i--) {
        const e = lotteryEntrantsChanged.current[i];
        switch (e.eventType) {
          case "INSERT": {
            let w_entry = winners[e.new.lottery_id];
            if (w_entry == null) {
              w_entry = []
              winners[e.new.lottery_id] = w_entry;
            }
            w_entry.push(e.new);
            w_entry.sort((a: any, b: any) => { return a.order_index - b.order_index });
            break;
          }
          case "DELETE": {
            delete winners[e.old.lottery_id];
            break;
          }
        }
        lotteryEntrantsChanged.current.splice(i, 1);
      }
      setWinners(new Object(winners));
      setLotteryEntrantChanged(false);
    }
  }, [lotteryEntrantChanged, winners]);

  useEffect(() => {
    if (supabase == null) {
      supabaseOnClient(supabaseUrl, supabaseAnonKey, supabaseEmail, supabasePassword, (data: AuthTokenResponsePassword, sb: SupabaseClient) => {
        if (data.error == null) {
          let channel = sb.channel("realtime all").on("postgres_changes", {
            event: "*",
            schema: "public",
            table: "entrants"
          }, ((payload) => {
            namesChanged.current.push(payload);
            setNameChanged(true);
          })).on("postgres_changes", {
            event: "*",
            schema: "public",
            table: "lotteries"
          }, setLotteryChanged).on("postgres_changes", {
            event: "*",
            schema: "public",
            table: "entrants_lotteries"
          }, ((payload) => {
            lotteryEntrantsChanged.current.push(payload);
            setLotteryEntrantChanged(true);
          })).subscribe();
          setSupabase(sb);
          return () => {
            sb.removeChannel(channel);
            setSupabase(null);
          };
        }
      });
    }
  }, []);

  return (
    <Box>
      <Divider
        sx={{ maxWidth: 650, mx: "auto", marginTop: "25px", marginBottom: "20px" }}
      >
        <Chip
          size="lg"
          sx={{ padding: "10px", paddingLeft: "20px", paddingRight: "20px" }}
        >
          <Typography level="title-lg">New Lottery</Typography>
        </Chip>
      </Divider>

      <Card
        variant="soft"
        size="lg"
        sx={{ alignItems: "center", width: "fit-content", mx: "auto", maxWidth: "650px", boxShadow: "lg" }}
      >
        <Box role="group" sx={{ justifyContent: "center", display: 'flex', flexWrap: "wrap", gap: 2 }}>
          <Card
            size="md"
            sx={{ paddingLeft: "25px", paddingBottom: "10px", paddingRight: "25px" }}
          >
            <TimeSelect setTimeType={setTimeType} />
          </Card>

          <Card
            size="md"
            sx={{ paddingLeft: "25px", paddingBottom: "10px", paddingRight: "25px" }}
          >
            <WinnerAmountSelect
              maxWinners={names.length - 1}
              setMaxWinners={setMaxWinners}
            />
          </Card>
        </Box>

        <Card>
          <NameSelect
            names={names}
            supabase={supabase}
          />
        </Card>

        <CardActions>
          <Button variant="outlined" color="neutral" startDecorator={<Add />} onClick={() => setAddNameOpen(true)}>
            Add Name
          </Button>
          <Button
            variant="solid"
            color="primary"
            style={{ minWidth: "100px" }}
            onClick={() => startLottery(supabase, names, lotteries, maxWinners, timeType, (r: string) => failedLottery(r, setSnackbar))}
          >
            Start
          </Button>
        </CardActions>
      </Card>

      <Divider
        sx={{ maxWidth: 650, mx: "auto", marginTop: "40px" }}
      >
        <Chip
          size="lg"
          sx={{ padding: "10px", paddingLeft: "20px", paddingRight: "20px" }}
        >
          <Typography level="title-lg">Results</Typography>
        </Chip>
      </Divider>

      <LotteryResults
        lotteries={lotteries}
        setLotteries={setLotteries}
        winners={winners}
        names={names}
        supabase={supabase}
      />

      <Divider
        sx={{ maxWidth: 650, mx: "auto", marginTop: "40px" }}
      >
        <Chip
          size="lg"
          sx={{ padding: "10px", paddingLeft: "20px", paddingRight: "20px" }}
        >
          <Typography level="title-lg">Statistics</Typography>
        </Chip>
      </Divider>

      <LotteryStats
        supabase={supabase}
      />

      <NewName
        supabase={supabase}
        addNameOpen={addNameOpen}
        setAddNameOpen={setAddNameOpen}
      />

      <Snackbar
        variant="solid"
        color={snackbar?.color}
        autoHideDuration={5000}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        open={snackbar != null}
        onClose={() => {
          setSnackbar(null);
        }}
      >
        {snackbar?.text}
      </Snackbar>
    </Box>
  )
}

function failedLottery(reason: string, setSnackbar: Function) {
  switch (reason) {
    case "selection": {
      setSnackbar({ text: "No active participants selected!", color: "warning" });
      break;
    }
    case "time": {
      setSnackbar({ text: "A lottery for this day and time already exists!", color: "warning" });
      break;
    }
  }
}
