'use client'

import Card from "@mui/joy/Card";
import { SupabaseClient } from "@supabase/supabase-js";
import List from "@mui/joy/List";
import Typography from "@mui/joy/Typography";
import ListItem from "@mui/joy/ListItem";
import Chip from "@mui/joy/Chip";
import Stack from "@mui/joy/Stack";
import CardActions from "@mui/joy/CardActions";
import Button from "@mui/joy/Button";
import { useEffect, useState } from "react";
import DeleteForever from "@mui/icons-material/DeleteForever";
import WarningRoundedIcon from '@mui/icons-material/WarningRounded';
import ModalDialog from "@mui/joy/ModalDialog";
import DialogTitle from "@mui/joy/DialogTitle";
import Modal from "@mui/joy/Modal";
import Divider from "@mui/joy/Divider";
import DialogContent from "@mui/joy/DialogContent";
import DialogActions from "@mui/joy/DialogActions";
import { getRandomStrings, nullifyLottery, setLotteryFinished, sortCheckLotteries } from "../Utils";
import { useInterval } from "../UseInterval";
import Box from "@mui/joy/Box";

const animationTextLength = 10;
const animationRowDelay = 75;
const animationStepDelay = 10;

export default function LotteryResults({ lotteries, setLotteries, winners, names, supabase } : { lotteries: any[], setLotteries: Function, winners: {[key: number]: any[]}, names: any[], supabase: SupabaseClient | null }) {
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [confirmData, setConfirmData] = useState<any>(null);
  const [progressTimers, setProgressTimers] = useState<any>({});
  const [universalTimer, setUniversalTimer] = useState(0);
  const [needTimer, setNeedTimer] = useState(false);

  sortCheckLotteries(lotteries);

  useInterval(() => {
    sortCheckLotteries(lotteries);
    setLotteries(Array.from(lotteries));
  }, 30000);

  if (lotteries.length > 0) {
    const progressEntries: any[] = Object.keys(progressTimers);
    for (const id of progressEntries) {
      if (!lotteries.find((e) => e.id == id)) {
        delete progressTimers[Number.parseInt(id)];
        setProgressTimers(new Object(progressTimers));
        if (!lotteries.find((e) => e.in_progress)) {
          setNeedTimer(false);
        }
        break;
      }
    }
  }
  else {
    const timerValues: any[] = Object.values(progressTimers);
    if (timerValues.length > 0) {
      setProgressTimers({});
      setNeedTimer(false);
    }
  }

  for (const lottery of lotteries) {
    const lotteryWinners = winners[lottery.id];
    let timer = progressTimers[lottery.id];
    if ((lottery.in_progress || timer != null) && lotteryWinners != null) {
      const trueWinnersLength = lotteryWinners.filter((w) => w.won || w.ticket).length;
      if (timer == null) {
        timer = {};
        progressTimers[lottery.id] = timer;
        
        timer.texts = getRandomStrings(trueWinnersLength, animationTextLength);
        timer.start = universalTimer;
        timer.end = universalTimer + trueWinnersLength * animationRowDelay + animationTextLength * animationStepDelay;
        setProgressTimers(new Object(progressTimers));
        setNeedTimer(true);
      }
      else if (trueWinnersLength != timer.texts.length) {
        timer.texts = [...timer.texts, ...getRandomStrings(trueWinnersLength - timer.texts.length, animationTextLength)];
        setProgressTimers(new Object(progressTimers));
      }
    }
  }

  useInterval(() => {
    const time = universalTimer + 1;
    for (const lottery of lotteries) {
      let timer = progressTimers[lottery.id];
      if (timer != null && time >= timer.end && lottery.in_progress) {
        lottery.in_progress = false;
        setNeedTimer(false);
        setLotteryFinished(lottery, supabase, () => {
          delete progressTimers[lottery.id];
          setProgressTimers(new Object(progressTimers));
        });
      }
    }
    setUniversalTimer(time);
  }, needTimer ? 33 : null); // 30 FPS

  return (
    <>
      <Box role="group" sx={{ justifyContent: "center", display: "flex", flexWrap: "wrap", gap: 2, marginTop: "20px", width: "fit-content", mx: "auto" }}>
        {
        lotteries.map((lottery) => {
          return (
            <Card
              size="md"
              key={`${lottery.id}_card`}
              variant="soft"
              sx={{
                bgcolor: lottery.active ? "background.level1" : "background.level3",
                boxShadow: "lg",
                alignItems: "center",
                mx: "auto"
              }}
            >
              <Typography level="title-lg" alignSelf="center">{lottery.target_time.toLocaleString('en-us', {  weekday: 'long' })} {lottery.timetype}</Typography>
              <Card
                size="md"
                variant="outlined"
                sx={{
                  bgcolor: lottery.active ? "background.level0" : "background.level1"
                }}
              >
                <Typography
                  id={`decorated-list-${lottery.id}`}
                  level="body-lg"
                  fontWeight="lg"
                  textAlign="center"
                >
                  Winners
                </Typography>
                <List
                  aria-labelledby={`decorated-list-${lottery.id}`}
                  marker="decimal"
                  component="ol"
                >
                  {winners[lottery.id]?.map((w: any) => {
                    const entrant = names.find((e) => e.name === w.entrant_name);
                    if (w.won || w.ticket) {
                      const timer = progressTimers[lottery.id];
                      let chipText = lottery.in_progress && universalTimer == 0 ? "" : `${w.ticket && !w.won ? "🎟️ " : ""}${w.ticket && w.won ? "⚜️ " : ""}${w.entrant_name}`;
                      let winnerColor = lottery.in_progress && universalTimer == 0 ? "grey" : `hsl(${entrant.color}, 90%, 45%, 1)`;
                      if (timer != null && universalTimer > 0) {
                        const progress = universalTimer - timer.start;
                        const rawSteps = Math.max((progress - Math.max(w.order_index * animationRowDelay, 0)), 0) / animationStepDelay;
                        const steps = Math.floor(rawSteps);

                        let timerText = timer.texts[w.order_index];
                        timerText = timerText.slice(timerText.length - 1 + (rawSteps > 0 && rawSteps % 1 == 0 ? 1 : 0)) + timerText.slice(0, timerText.length - 1);
                        timer.texts[w.order_index] = timerText;

                        const spacing = " ".repeat(timerText.length > 0 ? Math.max(steps - chipText.length, 0) : Math.max(animationTextLength * 2 - chipText.length - 1 - steps, 0));

                        chipText = chipText.slice(0, steps) + spacing + timerText;

                        if (timerText.length > 0) {
                          winnerColor = "grey";
                        }
                      }
                      return (
                        <ListItem
                          key={`${w.entrant_name}_${lottery.id}`}
                          sx={{ textAlign: "center" }}
                        >
                          <Chip
                            variant="plain"
                            sx={{ bgcolor: winnerColor, fontSize: 16, minWidth: 75, textAlign: "center", color: "white", padding: 1.25, paddingRight: 2, paddingLeft: 2, textShadow: "black 2px 2px 3px", whiteSpace: "pre-wrap", marginLeft: "10px" }}
                          >
                            {chipText}
                          </Chip>
                        </ListItem>
                      )
                    }
                  })}
                </List>
              </Card>

              {lottery.active && <CardActions
                  buttonFlex="0 1 100px"
                >
                <Button
                  variant="outlined"
                  color="danger"
                  fullWidth={false}
                  onClick={() => {
                    setConfirmData({
                      lottery: lottery,
                      winners: winners,
                      names: names
                    });
                    setConfirmOpen(true);
                  }}
                  startDecorator={<DeleteForever />}
                >
                  Nullify
                </Button>
              </CardActions>}
            </Card>
          )
        })}
      </Box>

      <Modal open={confirmOpen} onClose={() => {
        setConfirmData(null);
        setConfirmOpen(false);
      }}>
        <ModalDialog variant="outlined" role="alertdialog">
          <DialogTitle>
            <WarningRoundedIcon />
            Confirmation
          </DialogTitle>
          <Divider />
          <DialogContent>
            Are you sure you want to cancel the lottery result?
          </DialogContent>
          <DialogActions>
            <Button
              variant="solid"
              color="danger"
              onClick={() => {
                nullifyLottery(confirmData, supabase);
                setConfirmData(null);
                setConfirmOpen(false);
              }}
            >
              Nullify
            </Button>
            <Button
              variant="outlined"
              color="neutral"
              onClick={() => {
                setConfirmData(null);
                setConfirmOpen(false);
              }}
            >
              Cancel
            </Button>
          </DialogActions>
        </ModalDialog>
      </Modal>
    </>
  )
}
