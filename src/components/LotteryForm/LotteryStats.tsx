import { Refresh } from "@mui/icons-material";
import Button from "@mui/joy/Button";
import Card from "@mui/joy/Card";
import CardActions from "@mui/joy/CardActions";
import Table from "@mui/joy/Table";
import { SupabaseClient } from "@supabase/supabase-js";
import { useEffect, useState } from "react";

export default function LotteryStats({ supabase } : { supabase: SupabaseClient | null }) {
  const [updateStats, setUpdateStats] = useState(true);
  const [entrants, setEntrants] = useState<any[] | null>(null);

  useEffect(() => {
    if (updateStats && supabase != null) {
      supabase.rpc("stats").then((value) => {
        setEntrants(value.data?.sort((a: any, b: any) => a.entrant_name < b.entrant_name ? -1 : (a.entrant_name > b.entrant_name ? 1 : 0)));
      })
      setUpdateStats(false);
    }
  }, [supabase, updateStats]);

  return (
    <Card
      variant="soft"
      size="sm"
      sx={{ marginTop: "20px", alignItems: "center", width: "fit-content", mx: "auto", boxShadow: "lg" }}
    >
      <Table
        stripe="even"
        sx={{ width: "auto" }}
      >
        <thead>
          <tr>
            <th style={{ textAlign: "center" }}>Name</th>
            <th style={{ paddingLeft: "3px", paddingRight: "3px", textAlign: "center" }}>Current<br/>tickets</th>
            <th style={{ paddingLeft: "3px", paddingRight: "3px", textAlign: "center" }}>Tickets<br/>won</th>
            <th style={{ paddingLeft: "3px", paddingRight: "3px", textAlign: "center" }}>Lotteries<br/>won</th>
            <th style={{ paddingLeft: "3px", paddingRight: "3px", textAlign: "center" }}>Lottery<br/>win %</th>
            <th style={{ paddingLeft: "3px", paddingRight: "3px", textAlign: "center" }}>Lotteries<br/>entered</th>
          </tr>
        </thead>
        <tbody>
          {entrants?.map((e) => {
            return (
              <tr
                key={e.entrant_name}
              >
                <td style={{ textAlign: "center" }}>{e.entrant_name}</td>
                <td style={{ textAlign: "center" }}>{e.tickets}</td>
                <td style={{ textAlign: "center" }}>{e.tickets_won}</td>
                <td style={{ textAlign: "center" }}>{e.tickets_won + e.lotteries_won}</td>
                <td style={{ textAlign: "center" }}>{Math.round((((e.tickets_won + e.lotteries_won) / (e.lotteries_lost + e.lotteries_won + e.tickets_won) + Number.EPSILON) * 100) * 100) / 100} %</td>
                <td style={{ textAlign: "center" }}>{e.lotteries_lost + e.lotteries_won + e.tickets_used + e.tickets_won}</td>
              </tr>
            )
          })}
        </tbody>
      </Table>

      <CardActions
        buttonFlex="0 1 100px"
      >
        <Button
          variant="outlined"
          color="primary"
          fullWidth={false}
          onClick={() => setUpdateStats(true)}
          startDecorator={<Refresh />}
        >
          Refresh
        </Button>
      </CardActions>
    </Card>
  )
}
