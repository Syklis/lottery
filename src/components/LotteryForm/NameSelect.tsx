'use client'

import * as React from 'react';
import Checkbox from '@mui/joy/Checkbox';
import Chip from '@mui/joy/Chip';
import CheckIcon from '@mui/icons-material/Check';
import { SupabaseClient } from '@supabase/supabase-js';
import Box from '@mui/joy/Box';

export default function NameSelect({ names, supabase } : { names: any[], supabase: SupabaseClient | null }) {
  if (names.length == 0) {
    return (
      <p>No names yet, please add new names.</p>
    );
  }

  return (
    <Box role="group" sx={{ display: 'flex', flexWrap: "wrap", gap: 1 }}>
      {names.map((name) => {
        return (
          <Chip
            key={name.name}
            variant="plain"
            sx={{ outline: name.selected ? 2 : 0, bgcolor: `hsl(${name.color}, ${name.selected ? "90%" : "70%"}, ${name.selected ? "45%" : "70%"}, 1)`, minWidth: 75, textAlign: "center" }}
            startDecorator={
              name.selected && name.tickets <= 0 && <CheckIcon sx={{ zIndex: 1, pointerEvents: 'none', color: "white" }} />
            }
          >
            <Checkbox
              variant="outlined"
              color={name.selected ? 'primary' : 'neutral'}
              sx={{ color: "white", padding: 1, textShadow: "black 2px 2px 3px", fontStyle: name.selected ? "normal" : "italic" }}
              disableIcon
              overlay
              label={name.selected && name.tickets > 0 ? "🎟️ " + name.name : name.name}
              checked={name.selected}
              onChange={() => {
                if (supabase != null) {
                  supabase.from("entrants").update({
                    selected: !name.selected
                  }).eq("name", name.name).then(); // Doesn't work without then()???
                }
              }}
            />
          </Chip>
        )
      })}
    </Box>
  );
}
