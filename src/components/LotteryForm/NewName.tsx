'use client'

import Input from '@mui/joy/Input';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import ModalDialog from '@mui/joy/ModalDialog';
import Modal from '@mui/joy/Modal';
import DialogTitle from '@mui/joy/DialogTitle';
import RadioGroup from '@mui/joy/RadioGroup';
import { getRandomColors } from '../Utils';
import Chip from '@mui/joy/Chip';
import CheckIcon from '@mui/icons-material/Check';
import Radio from '@mui/joy/Radio';
import SupabaseClient from '@supabase/supabase-js/dist/module/SupabaseClient';
import Stack from '@mui/joy/Stack';
import { Dispatch, MutableRefObject, SetStateAction, useEffect, useState } from 'react';
import Button from '@mui/joy/Button';

export default function NewName({ supabase, addNameOpen, setAddNameOpen } : {supabase: SupabaseClient | null, addNameOpen: boolean, setAddNameOpen: Dispatch<SetStateAction<boolean>>}) {
  const [nameColor, setNameColor] = useState(0);
  const [newName, setNewName] = useState("");
  const [randomColors, setRandomColors] = useState<number[]>(getRandomColors(5));

  useEffect(() => {
    if (!addNameOpen) {
      newRandomColors(setRandomColors, setNameColor);
    }
  }, [addNameOpen]);

  return (
    <>
      <Modal
        open={addNameOpen}
        onClose={() => setAddNameOpen(false)}
      >
        <ModalDialog>
          <DialogTitle>Add a name</DialogTitle>
          <Stack spacing={2}>
            <FormControl>
              <FormLabel>Name</FormLabel>
              <Input onChange={(e) => setNewName(e.target.value)} autoFocus required />
            </FormControl>
            <FormControl>
              <FormLabel>Color</FormLabel>
              <RadioGroup
                name="best-movie"
                aria-labelledby="best-movie"
                orientation="horizontal"
                sx={{ flexWrap: 'wrap', gap: 1 }}
              >
                {randomColors.map((color) => {
                  const checked = nameColor === color;
                  return (
                    <Chip
                      key={color}
                      variant="plain"
                      sx={{ bgcolor: `hsl(${color}, 90%, 45%, 1)`, minWidth: 50 }}
                      startDecorator={
                        checked && <CheckIcon sx={{ zIndex: 1, pointerEvents: 'none', color: "white" }} />
                      }
                    >
                      <Radio
                        variant="outlined"
                        color={checked ? 'primary' : 'neutral'}
                        disableIcon
                        overlay
                        value={color}
                        checked={checked}
                        onChange={(event) => {
                          if (event.target.checked) {
                            setNameColor(color);
                          }
                        }}
                      />
                    </Chip>
                  )
                })}
              </RadioGroup>
            </FormControl>
            <Button onClick={() => newRandomColors(setRandomColors, setNameColor)}>New colors</Button>
            <Button onClick={() => addName(newName, nameColor, supabase, setAddNameOpen)}>Add</Button>
          </Stack>
        </ModalDialog>
      </Modal>
    </>
  );
}

function addName(newName: string, color: number, supabase: SupabaseClient | null, setAddNameOpen: Dispatch<SetStateAction<boolean>>) {
  if (supabase != null) {
    supabase.from("entrants").insert({
      name: newName,
      color: color
    }).then();
    setAddNameOpen(false);
  }
  else {
    console.error("No Supabase connection!");
  }
}

function newRandomColors(setRandomColors: Dispatch<SetStateAction<number[]>>, setNameColor: Dispatch<SetStateAction<number>>) {
  let newColors = getRandomColors(5);
  setRandomColors(newColors);
  setNameColor(newColors.at(0)!);
}
