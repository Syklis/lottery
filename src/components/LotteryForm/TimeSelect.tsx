import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import { FormHelperText, Radio, RadioGroup } from '@mui/joy';
import { ChangeEvent } from 'react';

export default function TimeSelect({ setTimeType }: { setTimeType: Function }) {
  const handleChange = ((e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.checked) {
      setTimeType(e.target.value);
    }
  })

  return (
    <FormControl>
      <FormLabel>Time</FormLabel>
      <RadioGroup defaultValue="afternoon" name="lottery-type">
        <Radio
          value="afternoon"
          label="Afternoon"
          key="afternoon"
          slotProps={{ input: { "aria-describedby": "afternoon-helper-text" } }}
          onChange={handleChange}
        />
        <FormHelperText id="evening-helper-text">{new Date().getHours() >= 18 ? "Next" : "This"} afternoon</FormHelperText>
        <Radio
          value="morning"
          label="Morning"
          key="morning"
          slotProps={{ input: { "aria-describedby": "morning-helper-text" } }}
          onChange={handleChange}
        />
        <FormHelperText id="morning-helper-text">{new Date().getHours() >= 9 ? "Next" : "This"} morning</FormHelperText>
      </RadioGroup>
    </FormControl>
  )
}
