import FormControl from '@mui/joy/FormControl/FormControl';
import FormLabel from '@mui/joy/FormLabel/FormLabel';
import Slider from '@mui/joy/Slider';

export default function WinnerAmountSelect({ maxWinners, setMaxWinners } : { maxWinners: number, setMaxWinners: Function }) {
  return (
    <FormControl>
      <FormLabel>Winners</FormLabel>
      <Slider
        sx={{ width: "130px", marginTop: "25px" }}
        min={1}
        max={maxWinners}
        valueLabelDisplay="on"
        onChange={(_event, value) => {
          setMaxWinners(value);
        }} />
    </FormControl>
  )
}
