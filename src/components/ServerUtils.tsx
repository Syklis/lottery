'use server'

import { cookies } from "next/headers";

export default async function authenticate(currentState: string, formData: FormData) {
  const password = formData.get("password")?.toString();
  if (password != null && await auth(password)) {
    cookies().set("auth", password, { httpOnly: true, secure: true, maxAge: 60 * 60 * 24 * 365 * 10 });
    return "success";
  }
  else {
    return "incorrect";
  }
}

export async function auth(auth: string | undefined) {
  return auth === process.env.ACCESS_PASSWORD!;
}
