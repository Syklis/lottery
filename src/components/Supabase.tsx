import { createClient } from "@supabase/supabase-js";

export async function supabaseOnServer(url: string, anonKey: string, email: string, password: string) {
  const supabase = createClient(url, anonKey);

  await supabase.auth.signInWithPassword({
    email: email,
    password: password
  });

  return supabase;
}

export function supabaseOnClient(url: string, anonKey: string, email: string, password: string, callback: Function) {
  const supabase = createClient(url, anonKey);

  supabase.auth.signInWithPassword({
    email: email,
    password: password
  }).then((e) => {
    callback(e, supabase);
  });
}
