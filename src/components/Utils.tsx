import { SupabaseClient } from "@supabase/supabase-js";

export function getRandomColors(amount: number): number[] {
  const results: number[] = [];
  for (let i = 0; i < amount; i++) {
    let candidate: number;
    do {
      candidate = Math.floor(Math.random() * 360);
    } while (results.includes(candidate)); // Try again if the same color already exists, maybe try filtering for similar as well?
    results.push(candidate);
  }
  return results;
}

export function getRandomStrings(amount: number, length: number): string[] {
  const characters = "!?@#¤%&/()=£$€\\<>*+-§"
  const results: string[] = [];
  for (let i = 0; i < amount; i++) {
    let result = "";
    for (let j = 0; j < length; j++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    results.push(result);
  }
  return results;
}

export function sortCheckLotteries(lotteries: any[]) {
  const now = new Date();
  for (const l of lotteries) {
    if (l.target_time == null) {
      const targetTime = new Date(l.created_at);
      const targetHour = l.timetype == "afternoon" ? 18 : 9;
      if (targetTime.getHours() >= targetHour) {
        targetTime.setDate(targetTime.getDate() + 1);
      }
      targetTime.setHours(targetHour);
      targetTime.setMinutes(0);
      targetTime.setSeconds(0);
      targetTime.setMilliseconds(0);
      l.target_time = targetTime;
    }

    l.active = now < l.target_time;
  }

  lotteries.sort((a, b) => b.target_time - a.target_time);
}

export function startLottery(supabase: SupabaseClient | null, names: any[], lotteries: any[], maxWinners: number, timeType: string, failedCallback: Function) {
  const maxTickets = 1;
  const ticketWinChance = 0.4; // Careful with the precision

  for (const l of lotteries) {
    if (l.active && l.timetype == timeType) {
      failedCallback("time");
      return;
    }
  }

  if (supabase !== null) {
    let entrants: any[] = [];
    let availableWinners = 0;
    const winners: any[] = [];

    // Ticket holders who are eligible to win.
    for (const n of names) {
      if (n.selected && n.tickets > 0) {
        availableWinners++;
        entrants.push({entrant: n, weight: 1});
      }
    }

    // If there are more or equal ticket holders than win slots, only eligible ticket holders will be able to win.
    // The rest will be able to gain new tickets, eligible to win originally or not.
    const ticketExclusive = entrants.length >= maxWinners;

    // Otherwise, we can just subtract these ticket holders from the lottery and give them auto-win.
    if (!ticketExclusive) {
      for (const e of entrants) {
        winners.push(e.entrant);
        availableWinners--;
        maxWinners--;
      }
      entrants = [];
    }

    // Go over the entrants who are not using a ticket.
    for (const n of names) {
      if (!n.selected || n.tickets == 0) {
        if (!ticketExclusive && n.selected) {
          availableWinners++;
        }
        if (n.selected || n.tickets < maxTickets) {
          entrants.push({entrant: n, weight: !ticketExclusive && n.selected ? 1 : ticketWinChance}); // Create a list of weighted entrants, note the precision.
        }
      }
    }

    if (availableWinners == 0) {
      failedCallback("selection");
      return;
    }

    maxWinners = Math.min(availableWinners, maxWinners);
    
    for (let i = 0; i < maxWinners; i++) {
      if (entrants.length > 0) {
        // The result is an array with the number of entries proportional to the weights.
        const lotteryTable = [];
        for (const e of entrants) {
          for (let i = 0; i < e.weight * 10; i++) { // The constant (10) should be equal in magnitude to the precision of the weight to get whole numbers.
            lotteryTable.push(e.entrant);
          }
        }

        const winner = lotteryTable[Math.floor(Math.random() * lotteryTable.length)];
        winners.push(winner);
        entrants = entrants.filter((e) => e.entrant !== winner);

        // If the winner was an extra ticket winner, we need to pick more winners.
        if (!winner.selected || (ticketExclusive && winner.tickets == 0)) {
          i--;
        }
      }
    }

    if (winners.length > 0) {
      supabase.from("lotteries").insert({
        timetype: timeType,
      }).select().then((e) => {
        if (e.data !== null) {
          const insertData = [];
          for (let i = 0; i < winners.length; i++) {
            const w = winners[i];
            const gotTicket = !w.selected || (ticketExclusive && w.tickets == 0);
            const usedTicket = w.selected && w.tickets > 0;
            insertData.push({
              entrant_name: w.name,
              lottery_id: e.data[0].id,
              ticket: gotTicket || usedTicket,
              won: !gotTicket,
              order_index: i
            });
            if (gotTicket) {
              supabase.from("entrants").update({
                tickets: w.tickets + 1
              }).eq("name", w.name).then();
            }
            else if (usedTicket) {
              supabase.from("entrants").update({
                tickets: w.tickets - 1
              }).eq("name", w.name).then();
            }
          }

          for (const entrant of entrants) {
            insertData.push({
              entrant_name: entrant.entrant.name,
              lottery_id: e.data[0].id,
              ticket: false,
              won: false,
              order_index: -1
            });
          }
          supabase.from("entrants_lotteries").insert(insertData).then();
        }
      });
    }
  }
}

export function nullifyLottery(data: any, supabase: SupabaseClient | null) {
  if (supabase !== null) {
    const lottery: any = data.lottery;
    const winners: {[key: number]: any[]} = data.winners;
    const names: any[] = data.names;
    
    supabase.from("entrants_lotteries").delete().eq("lottery_id", lottery.id).then(() => {
      supabase.from("lotteries").delete().eq("id", lottery.id).then();
    });
    for (const w of winners[lottery.id]) {
      const entrant = names.find((e) => e.name === w.entrant_name);
      if (entrant != null && w.ticket) {
        if (w.won) {
          supabase.from("entrants").update({
            tickets: entrant.tickets + 1
          }).eq("name", w.entrant_name).then();
        }
        else {
          supabase.from("entrants").update({
            tickets: entrant.tickets - 1
          }).eq("name", w.entrant_name).then();
        }
      }
    }
  }
}

export function setLotteryFinished(lottery: any, supabase: SupabaseClient | null, callback: Function) {
  if (supabase !== null) {
    supabase.from("lotteries").update({
      in_progress: false
    }).eq("id", lottery.id).then(() => callback());
  }
}
