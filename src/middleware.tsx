import { NextRequest, NextResponse } from "next/server"
import { auth } from "./components/ServerUtils"

export async function middleware(request: NextRequest) {
  const authed = await auth(request.cookies.get("auth")?.value);
  if (request.nextUrl.pathname != "/login") {
    if (!authed) {
      return NextResponse.redirect(new URL('/login', request.url));
    }
  }
  else if (authed) {
    return NextResponse.redirect(new URL('/', request.url));
  }
}

export const config = {
  matcher: ['/((?!api|_next/static|_next/image|.*\\.png$).*)']
}
