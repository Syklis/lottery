CREATE TABLE lotteries (
  id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  timetype TEXT NOT NULL,
  in_progress BOOLEAN NOT NULL DEFAULT true,
  created_at timestamptz NOT NULL DEFAULT now()
)