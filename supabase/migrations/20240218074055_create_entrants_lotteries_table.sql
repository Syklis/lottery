CREATE TABLE entrants_lotteries (
  entrant_name TEXT NOT NULL,
  lottery_id BIGINT NOT NULL,
  ticket BOOLEAN NOT NULL,
  won BOOLEAN NOT NULL,
  order_index INTEGER NOT NULL DEFAULT -1,
  FOREIGN KEY (entrant_name) REFERENCES entrants(name),
  FOREIGN KEY (lottery_id) REFERENCES lotteries(id),
  PRIMARY KEY (lottery_id, entrant_name)
)