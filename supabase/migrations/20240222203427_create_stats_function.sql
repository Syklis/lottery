CREATE FUNCTION stats() RETURNS json
  AS $$ SELECT
      json_agg(data)
    from
      (
        SELECT
          entrant_name,
          entrants.tickets,
          COUNT(*) FILTER (WHERE ticket = true AND won = false) AS tickets_won,
          COUNT(*) FILTER (WHERE ticket = true AND won = true) AS tickets_used,
          COUNT(*) FILTER (WHERE ticket = false AND won = true) AS lotteries_won,
          COUNT(*) FILTER (WHERE ticket = false AND won = false) AS lotteries_lost
        FROM
          entrants_lotteries
          INNER JOIN entrants ON entrants_lotteries.entrant_name = entrants.name
        GROUP BY
          entrants_lotteries.entrant_name,
          entrants.tickets
      ) AS data $$
  LANGUAGE SQL
  STABLE
